\documentclass{article}
\usepackage{amsmath}

\title{The Polarization of Electromagnetic Waves}

\begin{document}
\title{The Polarization of Electromagnetic Waves}
\author{Ian Waters}

\maketitle

Heretofore our treatment of laser beams and electromagnetic radiation in general  has been concerned primarily with \textit{linearly polarized} plane waves, that is, waves for which the orientation of the electric field vector $\vec E$ is constant as a function of both time and postion.  We observe that $\vec E$ as well as the wave vector $\vec k = \boldsymbol{e_k} \omega / c$ in the direction of the propagation of the wave reside in the same plane, commonly known as the \textit{plane of vibration}, throughout the passage of the wave.

In sections to follow we will take a more in-depth look at both linear polarization and polarization in general, as well as how we might use a powerful yet approachable mathematical tool known as the \textit{Jones calculus} to compute the resultant polarization of waves propagating through linear optical components such as wave plates and beam splitters.

\section{Linear Polarization}

We consider now a case where we have two waves propagting through a vacuum or some other homogenous, isotropic, and non-attenuating medium in the positive z-direction such that they share the same wave vector $\vec k = \boldsymbol{e_k} \omega / c$, wave number $k$, and angular frequency $\omega$, albeit with mutually orthogonal electric field component vectors  $\vec E_x$ and  $\vec E_y$, parallel to the x and y-axes, respectively.  Furthermore, we consider the general case where the waves are out of phase with one another, which we account for by introducing a phase shift $\phi$ in the wave given by  $\vec E_y$.  We  then express the two waves as 

\begin{align}
\vec E_x (z,t) =  \boldsymbol{e_x} E_{x0}  \exp[ i(kz-\omega t)]
\\
 and \quad \vec  E_y (z,t) =\boldsymbol{e_y} E_{y0}  \exp[ i(kz-\omega t + \phi)]
\end{align}

These two waves then combine to form a single wave $\vec E$, which is a simple vector summation of (1) and (2):

\begin{align}
\vec E (z,t) = \vec E_x (z,t) + \vec E_y (z,t)
\end{align}

This resultant wave is linearly polarized as well, and can be observed with its electric field vector $\vec E$  oscillating about a line rotated away from the x-axis by some  angle $\theta = \arctan(E_{y0} / E_{x0})$  in the xy-plane.  Furthermore, its amplitude $E$ can be calculated as 

\begin{align}
 E(z,t) = \sqrt{ [E_x(z,t)]^2+ [E_y(z,t)]^2}
\end{align}

It is important to emphasize here that this process may also be carried about in reverse, where we able to resolve any plane-polarized wave into two orthogonal components. 

We observe now that in the case where $\phi$  is zero or some even integer multiple of $\pi$, that equation (3) becomes

\begin{align}
\vec E (z,t) = (\boldsymbol{e_x} E_{x0} + \boldsymbol{e_y} E_{y0}) \exp[ i(kz-\omega t)]
\end{align}

and $\vec E_x$ and $\vec E_y$ are in phase.  

In the case where $\phi$  is some odd nonzero integer multiple of $\pi$, equation (3) becomes 

\begin{align}
\vec E (z,t) = (\boldsymbol{e_x} E_{x0} - \boldsymbol{e_y} E_{y0}) \exp[ i(kz-\omega t)]
\end{align}

\section{Circular Polarization}

Consider now a case where the waves $\vec E_x$ and $\vec E_y$ have equal magnitudes, say $E_{x0}= E_{y0}=  E_0$, with  phase shift $\phi = -\pi/2 \pm 2n\pi$, for some integer $n$.  We can then express the real parts of equations (1) and (2) as

\begin{align}
\vec E_x (z,t) =  \boldsymbol{e_x} E_0  \cos(kz-\omega t)
\\
 and \quad \vec  E_y (z,t) =\boldsymbol{e_y} E_0  \sin(kz-\omega t)
\end{align}

where the resultant wave  $\vec E$ is now 

\begin{align}
\vec E (z,t) = \boldsymbol{e_x} E_0  \cos(kz-\omega t) + \boldsymbol{e_y} E_0  \sin(kz-\omega t)
\end{align}

For some position on the $z$-axis, the magnitude of $\vec E$ remains fixed at $E_0$ for all times $t$, as the angle formed by $\vec E$ and the x-axis $\theta$ varies as $\theta = \omega t$.  To an observer situated on the positive x-axis to whom the wave is propagating,  $\vec E$ appears to be rotating clockwise, thus we say that the resultant wave is \textit{right-circularly polarized}. 

On the other hand, if the phase shift  $\phi = \pi/2 \pm 2n\pi$, for some integer $n$, the real parts of equations (1) and (2) become

\begin{align}
\vec E_x (z,t) =  \boldsymbol{e_x} E_0  \cos(kz-\omega t)
\\
 and \quad \vec  E_y (z,t) =- \boldsymbol{e_y} E_0  \sin(kz-\omega t)
\end{align}

and the resultant wave $\vec E$ is now

\begin{align}
\vec E (z,t) = \boldsymbol{e_x} E_0  \cos(kz-\omega t) - \boldsymbol{e_y} E_0  \sin(kz-\omega t)
\end{align}

To the same observer situated on the positive x-axis, $\vec E$ would now appear to be rotating clockwise, and we now say that the resultant wave is \textit{left-circularly polarized}.

Interestingly, a linearly polarized wave can in some conditions be created through the addition of two circularly polarized waves.  In particular, if we add the waves described in equations (9) and (12) above, we find that 

\begin{align}
\vec E (z,t) = \boldsymbol{e_x} 2E_0  \cos(kz-\omega t)
\end{align}

which is a linearly polarized wave of the same form as that of the real part of equation (1).

\section{Elliptical and Generalized Polarization}

In the preceding pages we first considered the case where the electric field vector $\vec E$ was free to change its magnitude as a function of time $t$,  as its angle with an arbitary axis remained constant (linear polarization).  In contrast, we then considered the case where the magnitude of $\vec E$ remain fixed as its angle changed at some constant angular speed $\omega$ (circular polarization).  It seems reasonable then to  consider a third case where we allow both the magntidue and angle of  $\vec E$ to vary over time, where the endpoint of  $\vec E$ will trace out an ellipse in a plane perpendicular to the progation of the wave.  

In order to show that the trace of  $\vec E$ is indeed an ellipse, we begin by again considering the magnitudes of the real parts of equations (1) and (2):  

\begin{align}
E_x (z,t) =  E_{x0}  \cos(kz-\omega t)
\\
 and \quad E_y (z,t) =E_{y0}  \cos(kz-\omega t + \phi)
\end{align}

We then use the sum formula for cosines to rewrite (15) as

\begin{align}
E_y (z,t) = E_{y0}  \cos(kz-\omega t)\ cos\phi- E_{y0} \sin(kz-\omega t)\ sin\phi
\end{align}

We then combine equations (14) and (16) to obtain

\begin{align}
\frac{E_y}{E_{y0}} - \frac{E_x}{E_{x0}} cos \phi = -\sin(kz-\omega t) sin \phi
\end{align}

It then follows from (14), that

\begin{align}
\sin( kz - \omega t ) = \sqrt{ 1 - \Big( \frac{E_x}{E_{x0}} \Big)^2  }
\end{align}

and

\begin{align}
\Big( \frac{E_y}{E_{y0}} - \frac{E_x}{E_{x0}} cos \phi \Big)^2= \Big[ 1 - \Big( \frac{E_x}{E_{x0}} \Big)^2  \Big] \sin^2 \phi
\end{align}

Finally, upon solving (19) for $\sin^2 \theta$ and rearranging, we find that:

\begin{align}
\Big( \frac{E_y}{E_{y0}}  \Big)^2 - \Big( \frac{E_x}{E_{x0}} \Big)^2 - 2 \Big( \frac{E_y}{E_{y0}}  \Big) \Big( \frac{E_x}{E_{x0}} \Big) \cos \phi  = \sin^2 \phi
\end{align}

which is the equation of an ellipse centered at the origin and rotated counterclockwise by some angle $\theta$, where 

\begin{align}
\theta = \frac{1}{2} tan^{-1} \Big( \frac{2 E_{x0} E_{y0} \cos \phi }{E_{x0}^2 - E_{y0}^2 } \Big)
\end{align}

Observe that in the case where $\phi$ is zero or some even integer multiple of $\pi$, that (20) reduces to

\begin{align}
E_y = \frac{E_{y0}}{E_{x0}} x
\end{align}

which is the equation of the same  line described in equation (5).

In the case where $\phi$ is some odd non-zero integer multiple of $\pi$, (20) reduces to

\begin{align}
E_y = -  \frac{E_{y0}}{E_{x0}} x
\end{align}

which is the equation of the line described in equation (5).

In the case where $E_{x0}= E_{y0}=  E_0$ and the phase shift  $\phi = \pm \pi/2 + 2n\pi$, for some integer $n$, 20 reduces to

\begin{align}
E_x^2 + E_y^2  = E_0^2
\end{align}

which is the equation of the same circles described in equations (9) and (12).

This effectively demonstrates that both linearly and circularly polarized waves are specific cases of a much more genera l\textit{elliptical polarization}.  Describing a given given wave as linearly, circularly, or elliptically polarized is known as its \textit{state of polarization}.

As we have seen, information about a particular state of polarization is specified by both the amplitude and phase of oscillation in the electric field vector $\vec E$ components  $\vec E_x$ and $\vec E_y$, given in a plane perpendicular to the propagation of the wave, as in equations (3). We can also express (3) equivalently as a two-dimensional \textit{Jones Vector}:

\begin{align}
\vec E  = \begin{bmatrix} E_x \\  E_y \end{bmatrix} = \begin{bmatrix} E_{x0} \exp{( i[kz-\omega t )]} \\  E_{y0} \exp{( i[kz-\omega t + \phi])} \end{bmatrix}
\end{align}

\section{s and p Polarizations}

In addition to Cartesian coordinates, another coordinate system employed often when working with polarization states is related to the \textit{plane of incidence} of a wave incident on a surface, defined as the plane formed by both the wave vector $\vec k$ and the surface normal vector.

In particular, the component of the electric field vector $\vec E$ parallel to the plane of incidence is designated the \textit{p-like} (for parallel), and the orthogonal component is referred to as \textit{s-like} (from \textit{senkrecht}, the German word for perpendicular).

\section{Natural Light}

Although the sort of laser beams typical of interferometers are in general polarized, more typical sources of electromagnetic radiation such as sunlight consist of a considerable number of randomly oriented atomic emitters, such that each excited atom will radiate a polarized wavetrain for approximately $10^{-8}$ seconds before returning to the ground state.  All emissions having the same frequency then will combine to produce a single resultant polarized wave, again only lasting for about $10^{-8}$ seconds.  Given that new wavetrains are constantly emitted randomly, the net result is that the overall polarization of a particular light source changes continuously in a completely unpredictable fashion.

\textit{Natural} or \textit{unpolarized light} consists of waves that feature changes in their overall polarization that are sufficiently fast as to render any overall resultant state of polarization indiscernable, and is typically represented as two orthogonal and linearly polarized waves, each of equal magnitude, with a relative phase difference that varies quickly and randomly.

In general, waves in realitiy are neither completely polarized or unpolarized, but instead feature an electric field vector that varies in a way that lies somewhere in between what would be typical of these two extreme cases, waves that we refer to as \textit{partially polarized}.  Partially polarized light is then typically represented using a combination of both polarized and unpolarized light.

\section{Polarizers}

A \textit{polarizer} is a type of optical filter which permits waves of a particular polarization to pass while restricting waves of other polarizations.  Polarizers may generally be divided into two categories: \textit{aborptive polarizers}, where undesired polarization states are absorbed by the device, and \textit{beam-splitting polarizers}, which divide an unpolarized wave into two waves with opposite polarization states.

Consider a  linearly polarized wave $\vec E_{in}$ with components $\vec E_x$ and $\vec E_y$ as given by the Jones vector:

\begin{align}
\vec E_{in}  = \begin{bmatrix} E_x \\  E_y \end{bmatrix}
\end{align}

Now consider a case where $\vec E_{in}$ encounters a simple horizontal linear polarizer  which serves to block the $\vec E_y$ componet of the wave, permitting only  $\vec E_x$ to pass through.  We can then express the emergent wave $\vec E_{out}$ as another Jones vector:

\begin{align}
\vec E_{out}  = \begin{bmatrix} E_x \\  0 \end{bmatrix}
\end{align}

This suggests that we might be able to relate $\vec E_{in}$ and $\vec E_{out}$ using an appropriate linear transformation, given by some $2 \times 2$ matrix $\boldsymbol{J}$, where $\boldsymbol{J}$ is the \textit{Jones Matrix} characteristic of the horizontal linear polarizer.

\begin{align}
\boldsymbol{J}  = \begin{bmatrix} m_{11}& m_{12} \\   m_{21}& m_{22} \end{bmatrix}
\end{align}

and

\begin{align}
\vec E_{in}  = \boldsymbol{J} \vec E_{out}
\end{align}

or

\begin{align}
\begin{bmatrix} E_x \\  E_y \end{bmatrix} = \begin{bmatrix} m_{11}& m_{12} \\   m_{21}& m_{22} \end{bmatrix} =  \begin{bmatrix} E_x \\  0 \end{bmatrix}
\end{align}

The components $m_{ii}$ are then easily found and we find finally that:

\begin{align}
\boldsymbol{J}  = \begin{bmatrix}1& 0 \\   0&0 \end{bmatrix}
\end{align}

This approach of using the product of Jones vectors and matrices to calculate the polarization of emergent waves from linear optical elements is known as \textit{Jones calculus}, as originally developed by physicist Robert Clark Jones in 1941.  It is important to note, however, that Jones calculus is only applicable to waves that are already fully polarized, and otherwise one must employ the more generally but appreciably more complex\textit{Mueller calculus},

\section{Jones Vectors}

Horizontal linearly polarized wave :

\begin{align}
\boldsymbol{J}  = \begin{bmatrix} 1 \\ 0 \end{bmatrix}
\end{align}

Vertical  linearly polarized wave :

\begin{align}
\boldsymbol{J}  = \begin{bmatrix} 0 \\ 1 \end{bmatrix}
\end{align}

Horizontal linearly polarized wave rotated by + 45$^{\circ}$ :

\begin{align}
\boldsymbol{J}  = \frac{\sqrt{2}}{2} \begin{bmatrix} 1 \\ 1 \end{bmatrix}
\end{align}

Horizontal linearly polarized wave rotated by - 45$^{\circ}$ :

\begin{align}
\boldsymbol{J}  = \frac{\sqrt{2}}{2} \begin{bmatrix} 1 \\ - 1 \end{bmatrix}
\end{align}

Right-circularly polarized wave:

\begin{align}
\boldsymbol{J}  = \frac{\sqrt{2}}{2} \begin{bmatrix} 1 \\ - i \end{bmatrix}
\end{align}

Left-circularly polarized wave:

\begin{align}
\boldsymbol{J}  = \frac{\sqrt{2}}{2} \begin{bmatrix} 1 \\  i \end{bmatrix}
\end{align}

\section{Jones Matrices for Selected Polarizers}

Linear vertical polarizer:

\begin{align}
\boldsymbol{J}  = \begin{bmatrix} 0 & 0 \\   0 & 1 \end{bmatrix}
\end{align}

Linear polarizer at + 45$^{\circ}$:

\begin{align}
\boldsymbol{J}  = \frac{1}{2} \begin{bmatrix}1 & 1 \\  1 & 1 \end{bmatrix}
\end{align}

Linear polarizer at - 45$^{\circ}$:

\begin{align}
\boldsymbol{J}  = \frac{1}{2} \begin{bmatrix}1 & - 1 \\  - 1 & 1 \end{bmatrix}
\end{align}

Circular polarizer, right-handed:

\begin{align}
\boldsymbol{J}  = \frac{1}{2} \begin{bmatrix}1 &  i \\  -  i & 1 \end{bmatrix}
\end{align}

Circular polarizer, left-handed:

\begin{align}
\boldsymbol{J}  = \frac{1}{2} \begin{bmatrix}1 & -  i \\  i & 1 \end{bmatrix}
\end{align}

The following represent Jones matrices for \textit{phase retarders}, optical elements which serve to introduce a phase shift between the horizontal and vertical components of $\vec E$.  Phase retarders typically feature birefringent uniaxial crystals with one crystal axis with a different index of refraction $n$.  This unique axis is referred to as the extraordinary or optic axis, an can be the fast or slow axis for the crystal depending on the type of crystal.  Waves propagate with a higher phase velocity along axes with a smaller refractive index (fast axis), whereas the speed of propagation is slower for axes with a higher refractive index (slow axis).  Note that any phase retarder with a fast axis equal to either the horizontal or vertical axis can be expressed  in the form

\begin{align}
\boldsymbol{J}  =  \begin{bmatrix} e^{i\phi_x} &  0 \\  0 & e^{i\phi_y} \end{bmatrix}
\end{align}

where $e^{i\phi_x}$ and $e^{i\phi_y}$ are the resulting phase shifts in $\vec E_x$ and $\vec E_x$ respectively.

Quarter-wave plate with vertical fast axis:

\begin{align}
\boldsymbol{J}  = e^{i\pi / 4} \begin{bmatrix}1 &  0 \\  0 & -  i \end{bmatrix}
\end{align}

Quarter-wave plate with horizontal fast axis:

\begin{align}
\boldsymbol{J}  = e^{ - i\pi / 4} \begin{bmatrix}1 &  0 \\  0 &  i \end{bmatrix}
\end{align}

Quarter-wave plate with fast axis rotated by angle $\theta$ w.r.t. the horizontal axis:

\begin{align}
\boldsymbol{J}  = e^{ - i\pi / 4} \begin{bmatrix} \cos^2 \theta + i \sin^2 \theta &  ( 1 - i ) \sin \theta \cos \theta \\   ( 1 - i ) \sin \theta \cos \theta &  \sin^2 \theta + i \cos^2 \theta \end{bmatrix}
\end{align}

Half-wave plate with fast axis rotated by angle $\theta$ w.r.t. the horizontal axis:

\begin{align}
\boldsymbol{J}  = e^{ - i\pi / 2} \begin{bmatrix} \cos 2 \theta  &  \sin 2 \theta  \\    \sin 2 \theta & - \cos 2 \theta \end{bmatrix}
\end{align}

\section{Jones Matrices for Beam Splitters}

from https://www.comsol.com/blogs/study-the-design-of-a-polarizing-beam-splitter-with-an-app/

When it comes to the design of a polarizing beam splitter, the most common configuration comes in the form of a cube. This cube design is valued as a viable alternative to the plate design for many reasons. Because there is only one reflecting surface in the cube configuration, it avoids producing ghost images. Further, as compared to the input beam, the translation of the transmitted output beam is quite small, which simplifies the process of aligning optical systems.

Let’s take a closer look at such a design. Polarizing beam splitter cubes are comprised of two prisms positioned at right angles. One of these prisms includes a dielectric coating evaporated on the intermediate hypotenuse surface. When a light wave enters the cube, the coating transmits the portion of the incident wave with the electric field that is polarized in the plane of incidence and reflects the portion of the incident wave with the electric field that is orthogonal to the plane of incidence. These parts of the incident wave are represented by p-polarization and s-polarization, respectively, in the schematic shown below.

from https://www.edmundoptics.com.au/resources/application-notes/optics/what-are-beamsplitters/:

Polarizing beamsplitters are designed to split light into reflected S-polarized and transmitted P-polarized beams. They can be used to split unpolarized light at a 50/50 ratio, or for polarization separation applications such as optical isolation (Figure 3).

from https://www.edmundoptics.com.au/optics/beamsplitters/polarizing-beamsplitters/:

Polarizing Beamsplitters are used to split unpolarized light into two polarized parts. Polarizing Beamsplitters are Beamsplitters designed to split light by polarization state rather than by wavelength or intensity. Polarizing Beamsplitters are often used in semiconductor or photonics instrumentation to transmit p-polarized light while reflecting s-polarized light. Polarizing Beamsplitters are typically designed for 0° or 45° angle of incidence with a 90° separation of the beams, depending on the configuration.

Edmund Optics offers a wide variety of Polarizing Beamsplitters in a range of configurations including plate, cube, or lateral displacement. Plate Beamsplitters are available in many sizes for optimized performance when used with ultraviolet, visible, or infrared wavelengths. Cube Beamsplitters are ideal for applications requiring durability or simplified mounting or system integration. Lateral Displacement Beamsplitters are designed to split the incident beam into two displaced parallel beams. Polarizing Beamsplitters are available that have been designed for common laser wavelengths or wavelength ranges.

References: 1. E. Hecht, Optics, 4th Ed., Pearson Addison Wesley, (2002) 325-330.
2. http://physics.wikia.com/wiki/Polarizer

Resources for polarizing beam splitters:

https://www.comsol.com/blogs/study-the-design-of-a-polarizing-beam-splitter-with-an-app/

\end{document}